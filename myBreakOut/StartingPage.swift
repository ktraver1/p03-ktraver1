//
//  StartingPage.swift
//  myBreakOut
//
//  Created by kevin travers on 2/15/16.
//  Copyright © 2016 BunnyPhantom. All rights reserved.
//

import SpriteKit

class StartingPage: SKScene {
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if let scene = GameScene(fileNamed:"GameScene") {
            // Configure the view.
            if let skView = self.view{
                skView.showsFPS = true
                skView.showsNodeCount = true
                
                /* Sprite Kit applies additional optimizations to improve rendering performance */
                skView.ignoresSiblingOrder = true
                
                /* Set the scale mode to scale to fit the window */
                scene.scaleMode = .AspectFill
                
                skView.presentScene(scene)
            }
            
        }
        
        
        
    }
}
