//
//  GameScene.swift
//  myBreakOut
//
//  Created by kevin travers on 2/15/16.
//  Copyright (c) 2016 BunnyPhantom. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate{
    var paddle:SKSpriteNode = SKSpriteNode()
    var ball:SKSpriteNode = SKSpriteNode()
    var bottomOfDeath:SKNode = SKNode()
    var block:SKShapeNode = SKShapeNode()
    var totalBlocksRow:Int = 5
    var totalBlocksColumn:Int = 5
    var totalBlocks:Int = 0
    var isGameOver = false
    let BallCategoryName = "ball"
    let PaddleCategoryName = "paddle"
    let BlockCategoryName = "block"
    let BlockNodeCategoryName = "blockNode"
    let BallCategory   : UInt32 = 0x1 << 0 // 00000000000000000000000000000001
    let BottomCategory : UInt32 = 0x1 << 1 // 00000000000000000000000000000010
    let BlockCategory  : UInt32 = 0x1 << 2 // 00000000000000000000000000000100
    let PaddleCategory : UInt32 = 0x1 << 3 // 00000000000000000000000000001000
    
    var blocks:SKSpriteNode = SKSpriteNode()
    var isTouching:Bool = false

    var gameOverLabel:SKLabelNode = SKLabelNode()
    var playAgainLabel:SKLabelNode = SKLabelNode()
    override func didMoveToView(view: SKView) {
        super.didMoveToView(view)
        gameOverLabel = childNodeWithName("gameOver") as! SKLabelNode
        gameOverLabel.text =  ""
        playAgainLabel = childNodeWithName("playAgain") as! SKLabelNode
        playAgainLabel.text =  ""
//border so doesnt bounce out of bounds
        totalBlocks = totalBlocksRow * totalBlocksColumn
        let border = SKPhysicsBody(edgeLoopFromRect: self.frame)
        border.friction = 0
        self.physicsBody = border
        physicsWorld.gravity = CGVectorMake(0, 0)
        physicsWorld.contactDelegate = self
        //create bottom so when ball hits it the player loses
        
        let bottomOfScreen = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 1)
        bottomOfDeath.physicsBody = SKPhysicsBody(edgeLoopFromRect: bottomOfScreen)
        addChild(bottomOfDeath)
        
        ball = childNodeWithName("ball") as! SKSpriteNode
        ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.frame.size.width/2)
        ball.physicsBody!.categoryBitMask = BallCategory
        
        // this defines the mass, roughness and bounciness
        ball.physicsBody!.allowsRotation = false
        ball.physicsBody!.friction = 0
        ball.physicsBody!.restitution = 1
        ball.physicsBody!.linearDamping = 0
        ball.physicsBody!.angularDamping = 0
        
        
        ball.physicsBody!.applyImpulse(CGVectorMake(5, -15))
       
        paddle = childNodeWithName("paddle") as! SKSpriteNode
        paddle.physicsBody = SKPhysicsBody(rectangleOfSize: paddle.frame.size)
        paddle.physicsBody!.allowsRotation = false
        paddle.physicsBody!.friction = 0.0
        paddle.physicsBody!.restitution = 1
        paddle.physicsBody!.linearDamping = 0
        paddle.physicsBody!.angularDamping = 0
        paddle.physicsBody!.dynamic = false
        paddle.physicsBody!.affectedByGravity = false
        
        //cattorgies for collsion detection
        //know which set of ojects collide with what
        bottomOfDeath.physicsBody!.categoryBitMask = BottomCategory
        ball.physicsBody!.categoryBitMask = BallCategory
        paddle.physicsBody!.categoryBitMask = PaddleCategory
        //sets up whats gonna happen when certain objects collide
        ball.physicsBody!.contactTestBitMask = BottomCategory | BlockCategory
        ball.physicsBody!.collisionBitMask = PaddleCategory | BlockCategory
        
        let blockWidth:CGFloat = CGFloat(100)
        let totalBlocksWidth = blockWidth * CGFloat(totalBlocksColumn)
        
        let padding: CGFloat = 10.0
        let totalPadding = padding * CGFloat(totalBlocksColumn - 1)
        
       
        let xOffset = (CGRectGetWidth(frame) - totalBlocksWidth - totalPadding) / 2
        
        
        for i in 0..<totalBlocksRow {
            for j in 0..<totalBlocksColumn {
                block = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 100, height: 20))
                block.fillColor = UIColor.redColor()
                block.strokeColor = UIColor.blackColor()
                
                block.position = CGPointMake(xOffset + CGFloat(CGFloat(j) + 0.5)*blockWidth + CGFloat(j-1)*padding, CGRectGetHeight(frame) * 0.60 + CGFloat(i * 40))
               
                block.physicsBody = SKPhysicsBody(rectangleOfSize: block.frame.size)
            
                block.physicsBody!.friction = 0.0
                block.physicsBody!.dynamic = false
                block.physicsBody!.allowsRotation = false
                block.physicsBody!.affectedByGravity = false
                block.name = BlockCategoryName
                block.physicsBody!.collisionBitMask = 0
                block.physicsBody!.categoryBitMask = BlockCategory
                addChild(block)
            }
        }

        
    }
    func didBeginContact(contact: SKPhysicsContact) {
        var objectOne: SKPhysicsBody
        var objectTwo: SKPhysicsBody
        
        // lower category is always stored in object one
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            objectOne = contact.bodyA
            objectTwo = contact.bodyB
        } else {
            objectOne = contact.bodyB
            objectTwo = contact.bodyA
        }
        
        // check whcihc collsion has occured
        if objectOne.categoryBitMask == BallCategory && objectTwo.categoryBitMask == BottomCategory {
            
            
            isGameOver = true
            let gameOverLabel = childNodeWithName("gameOver") as! SKLabelNode
            gameOverLabel.text =  "You Lose!!!"
            let playAgainLabel = childNodeWithName("playAgain") as! SKLabelNode
            playAgainLabel.text =  "Tap to Play"
            
            if let scene = StartingPage(fileNamed:"StartingPage") {
                // Configure the view.
                if let skView = view{
                    skView.showsFPS = true
                    skView.showsNodeCount = true
                    
                    /* Sprite Kit applies additional optimizations to improve rendering performance */
                    skView.ignoresSiblingOrder = true
                    
                    /* Set the scale mode to scale to fit the window */
                    scene.scaleMode = .AspectFill
                    
                    skView.presentScene(scene)
                }
                
            }
            
            
        }
        else if objectOne.categoryBitMask == BallCategory && objectTwo.categoryBitMask == BlockCategory{
            print("Block has been hit. Block down i reapet Block Down!!!")
            objectTwo.node?.removeFromParent()
            var numX = CGFloat(arc4random_uniform(UInt32(10 - 1))) + 1
            var numY = CGFloat((arc4random_uniform(UInt32(20 - 10))) + 1)
            ball.physicsBody!.applyImpulse(CGVectorMake(numX, -numY))
            removeObject()
            if isGameOver{
                gameOverLabel = childNodeWithName("gameOver") as! SKLabelNode
                gameOverLabel.text =  "Game Won!!!"
                //playAgainLabel = childNodeWithName("playAgain") as! SKLabelNode
                //playAgainLabel.text =  "Tap to Play"
                //self.scene?.view?.paused = true
                if let scene = StartingPage(fileNamed:"StartingPage") {
                    // Configure the view.
                    
                    if let skView = view{
                        skView.showsFPS = true
                        skView.showsNodeCount = true
                        
                        /* Sprite Kit applies additional optimizations to improve rendering performance */
                        skView.ignoresSiblingOrder = true
                        
                        /* Set the scale mode to scale to fit the window */
                        scene.scaleMode = .AspectFill
                        
                        skView.presentScene(scene)
                    }
                 
                    
                }

            }
        }
        else if objectOne.categoryBitMask == BallCategory && objectTwo.categoryBitMask == PaddleCategory{
            var numY = CGFloat((arc4random_uniform(UInt32(20 - 10))) + 1)
            var numX = CGFloat(arc4random_uniform(UInt32(10 - 1))) + 1
            ball.physicsBody!.applyImpulse(CGVectorMake(numX, -numY))
        }
    }
    func removeObject(){
        totalBlocks -= 1
        if totalBlocks <= 0{
            isGameOver = true
        }
    }
    
    func createBlocks(){
       //create blocks
        let blockWidth:CGFloat = CGFloat(100)
        let totalBlocksWidth = blockWidth * CGFloat(totalBlocksColumn)
        
        let padding: CGFloat = 10.0
        let totalPadding = padding * CGFloat(totalBlocksColumn - 1)
        
        
        let xOffset = (CGRectGetWidth(frame) - totalBlocksWidth - totalPadding) / 2
        
        for i in 0..<totalBlocksRow {
            for j in 0..<totalBlocksColumn {
                block = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 100, height: 20))
                block.fillColor = UIColor.redColor()
                block.strokeColor = UIColor.blackColor()
                
                block.position = CGPointMake(xOffset + CGFloat(CGFloat(j) + 0.5)*blockWidth + CGFloat(j-1)*padding, CGRectGetHeight(frame) * 0.60 + CGFloat(i * 40))
                
                block.physicsBody = SKPhysicsBody(rectangleOfSize: block.frame.size)
                
                block.physicsBody!.friction = 0.0
                block.physicsBody!.dynamic = false
                block.physicsBody!.allowsRotation = false
                block.physicsBody!.affectedByGravity = false
                block.name = BlockCategoryName
                block.physicsBody!.collisionBitMask = 0
                block.physicsBody!.categoryBitMask = BlockCategory
                addChild(block)
            }
        }

    }
    
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if isTouching{
            if !isGameOver{
                let touch = touches.first
                let location = touch!.locationInNode(self)
                var paddleX = location.x
                var timeDuration:Double = Double(paddle.position.x - paddleX)
                timeDuration = abs(timeDuration)/400
                
                paddleX = max(paddleX, paddle.size.width/2)
                paddleX = min(paddleX, size.width - paddle.size.width/2)
                let moveTo = SKAction.moveTo(CGPointMake(CGFloat(paddleX),CGRectGetMidY(self.frame)/4), duration: timeDuration)
                paddle.runAction(moveTo)
            }
            
        }
        
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if !isGameOver{
            
            isTouching = true
            let touch = touches.first
            let location = touch!.locationInNode(self)
            var paddleX = location.x
            paddleX = max(paddleX, paddle.size.width/2)
            paddleX = min(paddleX, size.width - paddle.size.width/2)
            var timeDuration:Double = Double(paddle.position.x - paddleX)
            timeDuration = abs(timeDuration)/400
            let moveTo = SKAction.moveTo(CGPointMake(CGFloat(paddleX),CGRectGetMidY(self.frame)/4), duration: timeDuration)
            paddle.runAction(moveTo)
        }
        
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        isTouching = false
        paddle.removeAllActions()
    }

   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
