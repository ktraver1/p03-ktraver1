//
//  GameScene.swift
//  breakOut
//
//  Created by kevin travers on 2/12/16.
//  Copyright (c) 2016 BunnyPhantom. All rights reserved.
//

import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    var paddle:SKShapeNode = SKShapeNode()
    var ball:SKSpriteNode = SKSpriteNode()
    
    let BallCategoryName = "ball"
    let PaddleCategoryName = "paddle"
    let BlockCategoryName = "block"
    let BlockNodeCategoryName = "blockNode"
    let BallCategory   : UInt32 = 0x1 << 0 // 00000000000000000000000000000001
    let BottomCategory : UInt32 = 0x1 << 1 // 00000000000000000000000000000010
    let BlockCategory  : UInt32 = 0x1 << 2 // 00000000000000000000000000000100
    let PaddleCategory : UInt32 = 0x1 << 3 // 00000000000000000000000000001000

    var blocks:SKSpriteNode = SKSpriteNode()
    var isTouching:Bool = false
    
    override func didMoveToView(view: SKView) {
       //border so doesnt bounce out of bounds
        /*
        let borderBody = SKPhysicsBody(edgeLoopFromRect: self.frame)
        borderBody.friction = 0
        self.physicsBody = borderBody
        physicsWorld.gravity = CGVectorMake(0, 0)
        //hysicsWorld.gravity = CGVectorMake(0, 0)

        var randomX:CGFloat = CGFloat(arc4random())
        var randomY:CGFloat = CGFloat(arc4random())
        */
        ball = self.childNodeWithName(BallCategoryName) as! SKSpriteNode
        
       
        //ball.speed = 10
        //ball.lineWidth = 4
        
        /*
        ball.position = CGPointMake(CGFloat(randomX%(self.frame.width)), CGFloat(randomY%(self.frame.height)))
        ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.frame.size.width/2)
        //ball.physicsBody!.categoryBitMask = BallCategory
        
        // this defines the mass, roughness and bounciness
        ball.physicsBody!.friction = 0
        ball.physicsBody!.applyImpulse(CGVectorMake(10, -10))
        */
        addChild(ball)

        /*
        ball.physicsBody!.restitution = 1
        ball.physicsBody!.linearDamping = 0
        ball.physicsBody!.angularDamping = 0
        ball.physicsBody!.mass = 0.5
        //ball.physicsBody!.applyImpulse(CGVectorMake(5, -15))
        
        // this will allow the balls to rotate when bouncing off each other
        ball.physicsBody!.allowsRotation = false
        //ball.physicsBody!.applyImpulse(CGVectorMake(10, -10))
       
      
        
        //paddle = SKSpriteNode(imageNamed:"Spaceship")
        paddle = SKShapeNode(rect: CGRect(x: 0, y: 0, width: 100, height: 20))
        paddle.fillColor = UIColor.redColor()
        paddle.strokeColor = UIColor.blackColor()
        paddle.position = CGPoint(x:CGRectGetMidX(self.frame), y:CGRectGetMidY(self.frame)/4)
        
        paddle.physicsBody = SKPhysicsBody(rectangleOfSize: paddle.frame.size)
        paddle.physicsBody!.allowsRotation = false
        paddle.physicsBody!.friction = 1.0
        paddle.physicsBody!.affectedByGravity = false
        paddle.name = BlockCategoryName
        paddle.physicsBody!.categoryBitMask = PaddleCategory
        
        self.addChild(paddle)
        */

        
      
    }
    override func touchesMoved(touches: Set<UITouch>, withEvent event: UIEvent?) {
        if isTouching{
            let touch = touches.first
            let location = touch!.locationInNode(self)
            var timeDuration:Double = Double(paddle.position.x - location.x)
            timeDuration = abs(timeDuration)/150
            let moveTo = SKAction.moveTo(CGPointMake(CGFloat(location.x),CGRectGetMidY(self.frame)/4), duration: timeDuration)
            paddle.runAction(moveTo)
        }
      
    }
   
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
       
        isTouching = true
        let touch = touches.first
        let location = touch!.locationInNode(self)
        var timeDuration:Double = Double(paddle.position.x - location.x)
        timeDuration = abs(timeDuration)/150
        let moveTo = SKAction.moveTo(CGPointMake(CGFloat(location.x),CGRectGetMidY(self.frame)/4), duration: timeDuration)
        paddle.runAction(moveTo)
        
    }
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
        isTouching = false
        paddle.removeAllActions()
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
